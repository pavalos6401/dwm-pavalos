#ifndef _CARBON_DARK_H
#define _CARBON_DARK_H

static const unsigned int baralpha    = 0xd0;
static const unsigned int borderalpha = OPAQUE;

static const char col_ignore[]  = "#000000";

static const char col_gray10[]  = "#f4f4f4";
static const char col_gray50[]  = "#8d8d8d";
static const char col_gray70[]  = "#525252";
static const char col_gray80[]  = "#393939";
static const char col_gray100[] = "#161616";

static const char col_blue50[] = "#4589ff";

static const char *colors[][3] = {
	/*                   fg           bg           border   */
	[SchemeNorm]     = { col_gray10,  col_gray100, col_gray80  },
	[SchemeSel]      = { col_gray100, col_gray50,  col_blue50  },
	[SchemeStatus]   = { col_gray10,  col_gray100, col_ignore  },
	[SchemeTagsNorm] = { col_gray10,  col_gray100, col_ignore  },
	[SchemeTagsSel]  = { col_gray100, col_gray50,  col_ignore  },
	[SchemeInfoNorm] = { col_gray10,  col_gray100, col_ignore  },
	[SchemeInfoSel]  = { col_gray10,  col_gray100, col_ignore  },
};

static const unsigned int alphas[][3]      = {
	/*                   fg      bg        border     */
	[SchemeNorm]     = { OPAQUE, baralpha, borderalpha },
	[SchemeSel]      = { OPAQUE, baralpha, borderalpha },
	[SchemeStatus]   = { OPAQUE, baralpha, borderalpha  },
	[SchemeTagsNorm] = { OPAQUE, baralpha, borderalpha  },
	[SchemeTagsSel]  = { OPAQUE, baralpha, borderalpha  },
	[SchemeInfoNorm] = { OPAQUE, baralpha, borderalpha  },
	[SchemeInfoSel]  = { OPAQUE, baralpha, borderalpha  },
};

static const char *fonts[]    = { "IBM Plex Sans:size=10" };
static const char dmenufont[] = "IBM Plex Mono:size=10";

static const char *const dmenu_nb = col_gray100;
static const char *const dmenu_nf = col_gray10;
static const char *const dmenu_sb = col_gray50;
static const char *const dmenu_sf = col_gray100;

#endif // !_CARBON_DARK_H
